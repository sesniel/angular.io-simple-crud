import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewSerComponent } from './new-ser.component';

describe('NewSerComponent', () => {
  let component: NewSerComponent;
  let fixture: ComponentFixture<NewSerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewSerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewSerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
