import { User } from './../user';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  @Input() user: User;
  @Output() updateUserEvent = new EventEmitter();
  userEdit: User = new User();

  constructor() { }

  ngOnInit() {
    Object.assign(this.userEdit, this.user);
    console.log(this.user);
  }

  update() {
    this.updateUserEvent.emit({original: this.user, edited:this.userEdit})
  }

}
